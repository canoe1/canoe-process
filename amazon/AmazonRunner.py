import time

from amazon.AmazonResearchScraper import AmazonResearchScraper as AmazonResearchScraper
from amazon.AmazonProductScraper import AmazonProductScraper as AmazonProductScraper
from common_tools import ProductScraper as ProductScraper, ResearchScraper


class AmazonRunner:

    def __init__(self, product=None):
        self.startTime = time.time()
        self.product = product
        self.amazon_researcher_scraper = AmazonResearchScraper()
        self.amazon_product_scraper = None
        self.urls = None
        self.browsed_product = None

    def run(self):
        self.amazon_researcher_scraper.get_data(self.product)
        ResearchScraper.formatFile(self.amazon_researcher_scraper)

        self.urls = self.amazon_researcher_scraper.urls
        product_cookies = self.amazon_researcher_scraper.product_cookies
        self.amazon_product_scraper = AmazonProductScraper(self.urls, product_cookies)

        ProductScraper.searchFromUrls(self.amazon_product_scraper)
        if len(self.amazon_product_scraper.data) != 0:
            self.browsed_product = ProductScraper.return_cheapest_product(self.amazon_product_scraper)
        ProductScraper.createJSON(self.amazon_product_scraper)
        ProductScraper.emptyUrlsFile(self.amazon_product_scraper)

        print("total time taken: ", str(time.time() - self.startTime) + " amazon " + self.product)

    def run_for_favorite(self, urls_list):
        ResearchScraper.formatFile(self.amazon_researcher_scraper, urls_list)
        self.urls = urls_list
        self.amazon_product_scraper = AmazonProductScraper(self.urls)
        ProductScraper.searchFromUrls(self.amazon_product_scraper)
        ProductScraper.createJSON(self.amazon_product_scraper)
        ProductScraper.emptyUrlsFile(self.amazon_product_scraper)

