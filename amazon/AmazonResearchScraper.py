import os
import time

import requests
from bs4 import BeautifulSoup
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


class AmazonResearchScraper:

    def __init__(self):
        self.urls = []
        self.amazonPage = os.getenv('AMAZON_SEARCH_ENDPOINT')
        self.product_cookies = None

    currentPath = os.path.dirname(__file__)

    def get_data(self, product):
        headers = {
            "User-Agent": os.getenv('USER_AGENT'),
            "DNT": os.getenv('AMAZON_DNT'),
            "Connection": os.getenv('AMAZON_CONNECTION'),
            "Upgrade-Insecure-Requests": os.getenv('AMAZON_UPGRADE_INSECURE_REQUEST'),
            "scheme": os.getenv('AMAZON_SCHEME'),
            "accept": os.getenv('AMAZON_ACCEPT'),
            "accept-encoding": os.getenv('AMAZON_ACCEPT_ENCODING'),
            "accept-language": os.getenv('AMAZON_ACCEPT_LANGUAGE'),
        }

        requested_page = self.amazonPage + product
        session = requests.Session()

        r = session.get(requested_page, headers=headers)
        self.product_cookies = session.cookies.get_dict()

        if r.status_code > 500:
            print("Page %s must have been blocked by Amazon: status code: %d" % (requested_page, r.status_code))
            return None

        content = r.content

        soup = BeautifulSoup(content, features="lxml")
        counter = 0
        for d in soup.findAll(os.getenv('AMAZON_PRODUCT_CARD_ELEMENT'), attrs={
            os.getenv('AMAZON_PRODUCT_CARD_SELECTOR_TYPE'): os.getenv('AMAZON_PRODUCT_CARD_SELECTOR_VALUE')}):
            if counter == 3:
                break
            else:
                if not d.find(os.getenv('AMAZON_SPONSORED_PRODUCT_ELEMENT'), attrs={
                    os.getenv('AMAZON_PRODUCT_SPONSORED_TYPE'): os.getenv('AMAZON_SPONSORED_PRODUCT_SELECTOR_VALUE')}):
                    url = d.find(os.getenv('AMAZON_PRODUCT_ELEMENT'), attrs={
                        os.getenv('AMAZON_PRODUCT_SELECTOR_TYPE'): os.getenv('AMAZON_PRODUCT_SELECTOR_VALUE')})
                    if url:
                        url = 'https://www.amazon.fr' + url.get('href')
                    else:
                        url = None
                    counter += 1

                    if url is not None:
                        self.urls.append(url)
