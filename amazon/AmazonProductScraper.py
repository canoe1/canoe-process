import ast
import os

from selectorlib import Extractor

from common_tools import ProductScraper
from dto.ProductDto import ProductDto
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


def formatImageUrl(dataDict):
    if type(dataDict['image']) is str:
        tmp_img = dataDict['image'].strip('][').split(', ')[0]
        tmp_img = ast.literal_eval(tmp_img)
        return list(tmp_img.keys())[0]
    return dataDict['image']


class AmazonProductScraper:

    def __init__(self, urls, cookies=None):
        self.no_urls = ProductScraper.countLines(self)
        self.data = [0] * self.no_urls
        self.site_name = "amazon"
        self.cookies = cookies
        self.urls = urls
        self.headers = {
            "User-Agent": os.getenv('USER_AGENT'),
            "DNT": os.getenv('AMAZON_DNT'),
            "Connection": os.getenv('AMAZON_CONNECTION'),
            "Upgrade-Insecure-Requests": os.getenv('AMAZON_UPGRADE_INSECURE_REQUEST'),
            "scheme": os.getenv('AMAZON_SCHEME'),
            "accept": os.getenv('AMAZON_ACCEPT'),
            "accept-encoding": os.getenv('AMAZON_ACCEPT_ENCODING'),
            "accept-language": os.getenv('AMAZON_ACCEPT_LANGUAGE'),
        }

    currentPath = os.path.dirname(__file__)
    e = Extractor.from_yaml_file(currentPath + '/amazonProduct.yml')

    def scrape(self, url, counter, q):

        r = ProductScraper.runRequest(self, url)
        if r.status_code > 500:
            print("Page %s must have been blocked by Amazon as the status code was %d" % (url, r.status_code))
            return None

        dataDict = AmazonProductScraper.e.extract(r.text)
        product = self.adapt_extracted_data(dataDict, url)
        if product:
            q.put({'counter': counter, 'data': product})

    @staticmethod
    def adapt_extracted_data(dataDict, url):
        dataDict['image'] = formatImageUrl(dataDict)

        dataDict['site'] = 'amazon'

        dataDict['url'] = url
        if dataDict['price'] is not None:
            del dataDict['rescue_price']
            del dataDict['multiple_vendors_price']
            del dataDict['sale_price']
        else:
            if dataDict['rescue_price'] is not None:
                dataDict['price'] = dataDict['rescue_price']
                del dataDict['rescue_price']
                del dataDict['multiple_vendors_price']
                del dataDict['sale_price']
            elif dataDict['sale_price'] is not None:
                dataDict['price'] = dataDict['sale_price']
                del dataDict['sale_price']
                del dataDict['rescue_price']
                del dataDict['multiple_vendors_price']
            else:
                dataDict['price'] = dataDict['multiple_vendors_price']
                del dataDict['rescue_price']
                del dataDict['multiple_vendors_price']
                del dataDict['sale_price']

        dataDict['short_description'] = ProductScraper.formatDesc(dataDict['short_description'])

        if dataDict['name'] is not None and dataDict['price']:
            return ProductDto(dataDict['name'], dataDict['price'], dataDict['short_description'], dataDict['image'],
                              dataDict['availability'], dataDict['rating'], dataDict['url'], dataDict['site'])
        return None
