class BasketDto:
    def __init__(self, products, site_name):
        self.site_name = site_name
        self.price = 0.0
        for product in products:
            if product is None:
                self.price = 0.0
            else:
                self.price += float('%.2f' % product.get('price'))
        self.products = products

    def get_price(self):
        return float(self.price)
