class FlightDto:
    def __init__(self, logo_url, price, d_hour, a_hour, duration, d_airport, a_airport, companies, stopover_list, row_url):
        self.logo_usr = logo_url
        self.price = price
        self.d_hour = d_hour
        self.a_hour = a_hour
        self.duration = duration
        self.d_airport = d_airport
        self.a_airport = a_airport
        self.companies = companies
        self.stopover_list = stopover_list
        self.row_url = row_url


    def print(self):
        print(self.__dict__)
