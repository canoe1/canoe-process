
class ProductDto:
    def __init__(self, name, price, description, image, availability, rating, url, site):
        self.name = name
        formatted_price = price.split('-')
        formatted_price = formatted_price[0].replace('EUR', '').replace('/pièce', '').replace('(livraison incluse)', '').replace('\xa0', '').replace('.HT','').replace(' ', '').replace('€','').replace(',', '.') if price is not None else None
        self.price = float(formatted_price)
        self.description = description
        self.image = image
        self.availability = availability
        self.rating = rating
        self.url = url
        self.site = site

    def get_price(self):
        return self.get('price')

