import ast
import json
import os
import multiprocessing as mp

from multiprocessing import Process

import requests

from BasketRunner import BasketRunner
from aliexpress.AliexpressRunner import AliexpressRunner
from amazon.AmazonProductScraper import AmazonProductScraper
from amazon.AmazonRunner import AmazonRunner
from boulanger.BoulangerProductScraper import BoulangerProductScraper
from boulanger.BoulangerRunner import BoulangerRunner
from cdiscount.CdiscountRunner import CdiscountRunner
from common_tools import ProductScraper, ResearchScraper
from dto.BasketDto import BasketDto
from ebay.EbayProductScraper import EbayProductScraper
from ebay.EbayResearchScraper import EbayResearchScraper as EbayResearchScraper
from ebay.EbayRunner import EbayRunner
from flights.FlightRunner import FlightRunner
from reverseSearch.ReverseSearchRunner import ReverseSearchRunner


def site_detection(urls_list):
    amazon_urls_list = []
    ebay_urls_list = []
    boulanger_urls_list = []
    cdiscount_urls_list = []
    aliexpress_urls_list = []

    is_amazon = False
    is_ebay = False
    is_boulanger = False
    is_cdiscount = False
    is_aliexpress = False

    count = 0

    for url in urls_list:
        if "amazon" in url:
            is_amazon = True
            amazon_urls_list.append(url)
        elif "ebay" in url:
            is_ebay = True
            ebay_urls_list.append(url)
        elif "cdiscount" in url:
            is_cdiscount = True
            cdiscount_urls_list.append(url)
        elif "boulanger" in url:
            is_boulanger = True
            boulanger_urls_list.append(url)
        elif "aliexpress" in url:
            is_aliexpress = True
            aliexpress_urls_list.append(url)

    if is_amazon is True:
        count += 1
    if is_boulanger is True:
        count += 1
    if is_aliexpress is True:
        count += 1
    if is_ebay is True:
        count += 1
    if is_cdiscount is True:
        count += 1
    return amazon_urls_list, aliexpress_urls_list, boulanger_urls_list, cdiscount_urls_list, ebay_urls_list, count


def update_progress(token, progress):
    headers = {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json; charset=UTF-8'
    }
    if progress is not None:
        data = '{"percentage":' + str(progress) + ' }'
        r2 = requests.put(os.getenv('API_PUT_SEARCH_PROGRESS'), headers=headers, data=data)


class MainRunner:

    @staticmethod
    def formatAPIInput(body):
        str = body.decode("UTF-8")
        dict = ast.literal_eval(str)

        type = dict['type']
        if 'enable_aliExpress' in dict and dict['enable_aliExpress'] == "true":
            enable_aliExpress = True
        else:
            enable_aliExpress = False
        return type, enable_aliExpress, dict

    def run(self, body):
        type, enable_aliExpress, dict = self.formatAPIInput(body)
        token = dict['token']
        if type.__eq__('single'):
            product_name = dict['product_name'].replace(' ', '+')
            self.run_single(token, enable_aliExpress, product_name)
            self.aggregateJSONs()
        elif type.__eq__('basket'):
            product_list = dict['basket'].split(";")
            self.run_basket(token, enable_aliExpress, product_list)
        elif type.__eq__('reverse'):
            file_path = dict['file_path']
            token = dict['token']
            self.run_for_reverse(enable_aliExpress, file_path, token)
            self.aggregateJSONs()
        elif type.__eq__('favorite'):
            url_list = dict['product_urls']
            self.run_favorite(url_list, token)
            #self.run_favorite(url_list)
            self.aggregateJSONs()
        elif type.__eq__('flight'):
            self.run_flight(dict, token)

    @staticmethod
    def run_single(token, enable_aliExpress, product_name, is_reverse=None):
        q = mp.Queue()
        p = {}
        process_count = 4

        if enable_aliExpress is True:
            if is_reverse is True:
                progress = 100 // 6
            else:
                progress = 100 // 5
        else:
            if is_reverse is True:
                progress = 100 // 5
            else:
                progress = 100 // 4

        if is_reverse is True:
            update_progress(token, progress)

        p[1] = Process(target=MainRunner.runAmazon, args=(product_name, token, progress, q))
        p[1].start()
        p[2] = Process(target=MainRunner.runEbay, args=(product_name, token, progress, q))
        p[2].start()
        p[3] = Process(target=MainRunner.runBoulanger, args=(product_name, token, progress, q))
        p[3].start()
        p[4] = Process(target=MainRunner.runCdiscount, args=(product_name, token, progress, q))
        p[4].start()

        if enable_aliExpress is True:
            p[5] = Process(target=MainRunner.runAliexpress, args=(product_name, token, progress, q))
            p[5].start()
            process_count += 1

        for i in range(1, process_count + 1):
            p[i].join()
        # p[i].close()

        while q.empty() is not True:
            item = q.get()

        del q

    @staticmethod
    def run_basket(token, enable_aliExpress, product_list):
        product_list = [product_name.replace(' ', '+') for product_name in product_list]
        all_basket_list = []

        if enable_aliExpress is True:
            progress = 100 // 5
        else:
            progress = 100 // 4

        amazon_basket_runner = BasketRunner(product_list, MainRunner.runAmazon, "amazon")
        ebay_basket_runner = BasketRunner(product_list, MainRunner.runEbay, "ebay")
        boulanger_basket_runner = BasketRunner(product_list, MainRunner.runBoulanger, "boulanger")
        cdiscount_basket_runner = BasketRunner(product_list, MainRunner.runCdiscount, "cdiscount")

        q2 = mp.Queue()
        p = {}
        process_count = 4
        p[1] = Process(target=MainRunner.runForBasket, args=(token, progress, amazon_basket_runner, q2))
        p[1].start()
        p[2] = Process(target=MainRunner.runForBasket, args=(token, progress, ebay_basket_runner, q2))
        p[2].start()
        p[3] = Process(target=MainRunner.runForBasket, args=(token, progress, boulanger_basket_runner, q2))
        p[3].start()
        p[4] = Process(target=MainRunner.runForBasket, args=(token, progress, cdiscount_basket_runner, q2))
        p[4].start()

        if enable_aliExpress:
            aliexpress_basket_runner = BasketRunner(product_list, MainRunner.runAliexpress, "aliexpress")
            p[5] = Process(target=MainRunner.runForBasket, args=(token, progress, aliexpress_basket_runner, q2))
            p[5].start()
            process_count += 1

        for i in range(1, process_count + 1):
            p[i].join(timeout=28)

        while q2.empty() is not True:
            item = q2.get()
            if item is not None:
                all_basket_list.append(item)

        while None in all_basket_list:
            all_basket_list.remove(None)

        all_basket_list.sort(key=BasketDto.get_price)
        counter = 0
        for basket in all_basket_list:
            if all_basket_list[counter].price.__eq__(0.0):
                counter += 1

        current_dir = os.path.dirname(__file__)
        with open(current_dir + '/res.json', 'w') as res_file:
            if all_basket_list[counter] is not None:
                dict_data = all_basket_list[counter].__dict__
                json.dump(dict_data, res_file)
            res_file.close()

    @staticmethod
    def run_favorite(urls_list, token):
        urls_list = urls_list.split('$-_.+!*\'(),')

        amazon_urls_list, aliexpress_urls_list, boulanger_urls_list, cdiscount_urls_list, ebay_urls_list , site_count= site_detection(
            urls_list)
        q = mp.Queue()
        p = {}
        process_count = 0
        if site_count > 0:
            progress = 100 // site_count
        else:
            progress = 100

        if amazon_urls_list:
            p[process_count + 1] = Process(target=MainRunner.run_favorite_amazon, args=(amazon_urls_list, token,  progress, q))
            p[process_count + 1].start()
            process_count += 1

        if ebay_urls_list:
            p[process_count + 1] = Process(target=MainRunner.run_favorite_ebay, args=(ebay_urls_list, token,  progress, q))
            p[process_count + 1].start()
            process_count += 1

        if boulanger_urls_list:
            p[process_count + 1] = Process(target=MainRunner.run_favorite_boulanger, args=(boulanger_urls_list, token,  progress, q))
            p[process_count + 1].start()
            process_count += 1

        if cdiscount_urls_list:
            p[process_count + 1] = Process(target=MainRunner.run_favorite_cdiscount, args=(cdiscount_urls_list, token,  progress, q))
            p[process_count + 1].start()
            process_count += 1

        if aliexpress_urls_list:
            p[process_count + 1] = Process(target=MainRunner.run_favorite_aliexpress, args=(aliexpress_urls_list, token, progress, q))
            p[process_count + 1].start()
            process_count += 1

        for i in range(1, process_count + 1):
            p[i].join()
            # p[i].close()

        while q.empty() is not True:
            item = q.get()

        del q

    @staticmethod
    def run_favorite_amazon(amazon_urls_list, token=None, progress=None, q=None):
        amazon_runner = AmazonRunner()
        amazon_runner.run_for_favorite(amazon_urls_list)
        update_progress(token, progress)
        q.put(amazon_runner.browsed_product)

    @staticmethod
    def run_favorite_ebay(ebay_urls_list, token=None, progress=None, q=None):
        ebay_runner = EbayRunner()
        ebay_runner.run_for_favorite(ebay_urls_list)
        update_progress(token, progress)
        q.put(ebay_runner.browsed_product)

    @staticmethod
    def run_favorite_boulanger(boulanger_urls_list, token=None, progress=None, q=None):
        boulanger_runner = BoulangerRunner()
        boulanger_runner.run_for_favorite(boulanger_urls_list)
        update_progress(token, progress)
        q.put(boulanger_runner.browsed_product)

    @staticmethod
    def run_favorite_cdiscount(cdiscount_urls_list, token=None, progress=None, q=None):
        cdiscount_runner = CdiscountRunner()
        cdiscount_runner.run_for_favorite(cdiscount_urls_list)
        update_progress(token, progress)
        q.put(cdiscount_runner.browsed_product)

    @staticmethod
    def run_favorite_aliexpress(aliexpress_urls_list, token=None, progress=None, q=None):
        aliexpress_runner = AliexpressRunner()
        aliexpress_runner.run_for_favorite(aliexpress_urls_list)
        update_progress(token, progress)
        q.put(aliexpress_runner.browsed_product)

    @staticmethod
    def runForBasket(token, progress, site_basket_runner, q2):
        product_dto_list = []
        q = mp.Queue()
        p = {}
        product_counter = 1
        for product_name in site_basket_runner.product_list:
            p[product_counter] = Process(target=site_basket_runner.runner, args=(product_name, token, None, q))
            p[product_counter].start()
            product_counter += 1

        for i in range(1, product_counter):
            p[i].join(timeout=26)
            # p[i].join()

        while q.empty() is not True:
            item = q.get()
            if item is not None:
                product_dto_list.append(item)
        del q

        if len(product_dto_list) == product_counter - 1:
            basketDto = BasketDto(product_dto_list, site_basket_runner.site_name)
            q2.put(basketDto)
        else:
            q2.put(None)

        update_progress(token, progress)

    @staticmethod
    def runAmazon(product_name=None, token=None, progress=None, q=None):
        amazon_runner = AmazonRunner(product_name)
        amazon_runner.run()

        update_progress(token, progress)

        q.put(amazon_runner.browsed_product)

    @staticmethod
    def runEbay(product_name=None, token=None, progress=None, q=None):
        ebay_runner = EbayRunner(product_name)
        ebay_runner.run()

        update_progress(token, progress)

        q.put(ebay_runner.browsed_product)

    @staticmethod
    def runBoulanger(product_name=None, token=None, progress=None, q=None):
        boulanger_runner = BoulangerRunner(product_name)
        boulanger_runner.run()
        update_progress(token, progress)

        q.put(boulanger_runner.browsed_product)

    @staticmethod
    def runCdiscount(product_name=None, token=None, progress=None, q=None):
        cdiscount_runner = CdiscountRunner(product_name)
        cdiscount_runner.run()

        update_progress(token, progress)

        q.put(cdiscount_runner.browsed_product)

    @staticmethod
    def runAliexpress(product_name=None, token=None, progress=None, q=None):
        aliexpress_runner = AliexpressRunner(product_name)
        aliexpress_runner.run()

        update_progress(token, progress)
        q.put(aliexpress_runner.browsed_product)

    def run_for_reverse(self, enable_aliexpress, file_path, token):
        reverse_search_runner = ReverseSearchRunner(enable_aliexpress, file_path, token)
        reverse_search_runner.run()

    @staticmethod
    def aggregateJSONs():
        current_dir = os.path.dirname(__file__)
        output_files = []
        for root, dirs, files in os.walk(current_dir):
            for file in files:
                if file.endswith("output.json"):
                    output_files.append(os.path.join(root, file))

        with open(current_dir + '/res.json', 'w') as res_file:
            res_file.write('[]')

        with open(current_dir + '/res.json') as merged_data:
            data1 = json.loads(merged_data.read())

        for file in output_files:
            with open(file, 'r') as f:
                data2 = json.load(f)
                for product in data2:
                    data1.append(product)

        for file in output_files:
            with open(file, 'w') as f:
                f.truncate(0)
                f.write('{}')
                f.close()

        with open(current_dir + '/res.json', 'w') as outfile:
            while 0 in data1:
                data1.remove(0)
            data1.sort(key=lambda x: x['price'], reverse=False)
            json.dump(data1, outfile)

    @staticmethod
    def run_flight(dict, token):
        flight_runner = FlightRunner(token)
        flight_runner.run(dict)
