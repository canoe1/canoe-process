import time

from common_tools import ProductScraper, ResearchScraper
from ebay.EbayResearchScraper import EbayResearchScraper as EbayResearchScraper
from ebay.EbayProductScraper import EbayProductScraper as EbayProductScraper


class EbayRunner:

    def __init__(self, product=None):
        self.startTime = time.time()
        self.product = product
        self.ebay_researcher_scraper = EbayResearchScraper()
        self.ebay_product_scraper = None
        self.urls = None
        self.browsed_product = None

    def run(self):
        self.ebay_researcher_scraper.get_data(self.product)
        ResearchScraper.formatFile(self.ebay_researcher_scraper)

        self.urls = self.ebay_researcher_scraper.urls
        product_cookies = self.ebay_researcher_scraper.product_cookies
        self.ebay_product_scraper = EbayProductScraper(self.urls, product_cookies)

        ProductScraper.searchFromUrls(self.ebay_product_scraper)
        if len(self.ebay_product_scraper.data) != 0:
            self.browsed_product = ProductScraper.return_cheapest_product(self.ebay_product_scraper)
        ProductScraper.createJSON(self.ebay_product_scraper)
        ProductScraper.emptyUrlsFile(self.ebay_product_scraper)

        print("total time taken: ", str(time.time() - self.startTime) + " ebay " + self.product)

    def run_for_favorite(self, urls_list):
        ResearchScraper.formatFile(self.ebay_researcher_scraper, urls_list)
        self.urls = urls_list
        self.ebay_product_scraper = EbayProductScraper(self.urls)
        ProductScraper.searchFromUrls(self.ebay_product_scraper)
        ProductScraper.createJSON(self.ebay_product_scraper)
        ProductScraper.emptyUrlsFile(self.ebay_product_scraper)
