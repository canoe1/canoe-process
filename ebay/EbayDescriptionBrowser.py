import requests
from bs4 import BeautifulSoup


class EbayDescriptionBrowser:
    def __init__(self, cookies, url):
        self.cookies = cookies
        self.url = url

    def browse(self):
        headers = {
            'Authority': 'www.ebay.com',
            'Pragma': 'no-cache',
            'Cache-control': 'no-cache',
            'dDnt': '1',
            'Upgrade-insecure-requests': '1',
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36",
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-language': 'en-GB,en-US;q=0.9,en;q=0.8'
        }

        r = requests.get(self.url, headers=headers, cookies=self.cookies)

        content = r.content
        soup = BeautifulSoup(content, features="html.parser")

        description = soup.find('div', attrs={'id': 'ds_div'})
        description = u''.join(description.findAll(text=True))
        description = description.replace('\n', '')
        description = description.replace('\t', '')
        return description
