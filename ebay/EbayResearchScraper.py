import os
import time

import requests
from bs4 import BeautifulSoup
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


class EbayResearchScraper:

    def __init__(self):
        self.urls = []
        self.ebayPage = os.getenv('EBAY_SEARCH_ENDPOINT')
        self.product_cookies = None

    currentPath = os.path.dirname(__file__)

    def get_data(self, product):
        headers = {'Authority': os.getenv('EBAY_AUTHORITY'),
                   'Pragma': os.getenv('EBAY_PRAGMA'),
                   'Cache-control': os.getenv('EBAY_CACHE_CONTROL'),
                   'Dnt': os.getenv('EBAY_DNT'),
                   'Upgrade-insecure-requests': os.getenv('EBAY_UPGRADE_INSECURE_REQUESTS'),
                   "User-Agent": os.getenv('USER_AGENT'),
                   'Accept': os.getenv('EBAY_ACCEPT'),
                   'Accept-language': os.getenv('EBAY_ACCEPT_LANGUAGE')
                   }

        requested_page = self.ebayPage + product

        session = requests.Session()

        r = session.get(requested_page, headers=headers)
        self.product_cookies = session.cookies.get_dict()

        content = r.content
        soup = BeautifulSoup(content, features=os.getenv('EBAY_SOUP_PARSER'))
        counter = 0
        time.sleep(2)
        if soup.find(os.getenv('EBAY_PRODUCT_CARD_ELEMENT'), attrs={os.getenv('EBAY_PRODUCT_CARD_SELECTOR_TYPE'): os.getenv('EBAY_PRODUCT_CARD_SELECTOR_VALUE')}):
            for d in soup.findAll(os.getenv('EBAY_PRODUCT_CARD_ELEMENT'), attrs={os.getenv('EBAY_PRODUCT_CARD_SELECTOR_TYPE'): os.getenv('EBAY_PRODUCT_CARD_SELECTOR_VALUE')}):
                if counter == 3:
                    break
                else:
                    url = d.find(os.getenv('EBAY_PRODUCT_ELEMENT'), attrs={os.getenv('EBAY_PRODUCT_SELECTOR_TYPE'): os.getenv('EBAY_PRODUCT_SELECTOR_VALUE')})
                    url = url.get('href')
                    counter += 1

                    if url is not None:
                        self.urls.append(url)

        elif soup.find(os.getenv('EBAY_PRODUCT_CARD_ELEMENT2'), attrs={os.getenv('EBAY_PRODUCT_CARD_SELECTOR_TYPE2'): os.getenv('EBAY_PRODUCT_CARD_SELECTOR_VALUE2')}):
            for d in soup.findAll(os.getenv('EBAY_PRODUCT_CARD_ELEMENT2'), attrs={
                os.getenv('EBAY_PRODUCT_CARD_SELECTOR_TYPE2'): os.getenv('EBAY_PRODUCT_CARD_SELECTOR_VALUE2')}):
                if counter == 3:
                    break
                else:
                    if d.find(os.getenv('EBAY_PRODUCT_ELEMENT2'), attrs={os.getenv('EBAY_PRODUCT_SELECTOR_TYPE2'): os.getenv('EBAY_PRODUCT_SELECTOR_VALUE2')}):
                        url = d.find(os.getenv('EBAY_PRODUCT_ELEMENT2'), attrs={os.getenv('EBAY_PRODUCT_SELECTOR_TYPE2'): os.getenv('EBAY_PRODUCT_SELECTOR_VALUE2')})
                        url = url.get('href')
                        counter += 1

                        if url is not None:
                            self.urls.append(url)