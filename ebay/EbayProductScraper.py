import os

from selectorlib import Extractor

from common_tools import ProductScraper
from dto.ProductDto import ProductDto
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


class EbayProductScraper:

    def __init__(self, urls, cookies=None):
        self.no_urls = ProductScraper.countLines(self)
        self.data = [0] * self.no_urls
        self.site_name = "ebay"
        self.cookies = cookies
        self.urls = urls
        self.headers = {
            'Authority':os.getenv('EBAY_AUTHORITY'),
            'Pragma': os.getenv('EBAY_PRAGMA'),
            'Cache-control': os.getenv('EBAY_CACHE_CONTROL'),
            'Dnt': os.getenv('EBAY_DNT'),
            'Upgrade-insecure-requests': os.getenv('EBAY_UPGRADE_INSECURE_REQUESTS'),
            "User-Agent": os.getenv('USER_AGENT'),
            'Accept': os.getenv('EBAY_ACCEPT'),
            'Accept-language': os.getenv('EBAY_ACCEPT_LANGUAGE')
        }

    currentPath = os.path.dirname(__file__)
    e = Extractor.from_yaml_file(currentPath + '/ebayProduct.yml')

    def scrape(self, url, counter, q):

        r = ProductScraper.runRequest(self, url)

        dataDict = EbayProductScraper.e.extract(r.text)
        product = self.adapt_extracted_data(dataDict, url)

        if product:
            q.put({'counter': counter, 'data': product})

    @staticmethod
    def adapt_extracted_data(dataDict, url):
        dataDict['url'] = url
        dataDict['name'] = dataDict['name'].replace('Détails sur ', '')

        dataDict['site'] = 'ebay'

        # dataDict['short_description'] = EbayDescriptionBrowser(self.cookies, dataDict['short_description']).browse()

        if dataDict['rescue_price'] is not None:
            if dataDict['availability'] is None:
                dataDict['availability'] = '1'

        if dataDict['price']:
            if dataDict['converted_price']:
                dataDict['price'] = dataDict['converted_price']
            elif dataDict['converted_price_immediate']:
                dataDict['price'] = dataDict['converted_price_immediate']
            del dataDict['rescue_price']
            del dataDict['converted_price']
            del dataDict['converted_price_immediate']

        elif dataDict['rescue_price']:
            dataDict['price'] = dataDict['rescue_price']
            if dataDict['converted_price']:
                dataDict['price'] = dataDict['converted_price']
            elif dataDict['converted_price_immediate']:
                dataDict['price'] = dataDict['converted_price_immediate']
            del dataDict['converted_price']
            del dataDict['rescue_price']
            del dataDict['converted_price_immediate']

        dataDict['short_description'] = ProductScraper.formatDesc(dataDict['short_description'])

        if dataDict['name'] is not None and dataDict['price']:
            return ProductDto(dataDict['name'], dataDict['price'], dataDict['short_description'], dataDict['image'],
                              dataDict['availability'], dataDict['rating'], dataDict['url'], dataDict['site'])
        return None
