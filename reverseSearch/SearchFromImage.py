import os

import requests
from bs4 import BeautifulSoup


class SearchFromImage:

    def __init__(self, multipart):
        self.urls = []
        self.googlePage = 'http://www.google.hr/searchbyimage/upload'
        self.multipart = multipart
        self.key_words = None

    currentPath = os.path.dirname(__file__)

    def get_data(self):
        headers = {"User-Agent": os.getenv('USER_AGENT'),
                   "Accept-Encoding":"gzip, deflate",
                   "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                   "Dnt": os.getenv('GOOGLE_IMAGE_DNT'),
                   "Connection": os.getenv('GOOGLE_IMAGE_CONNECTION'),
                   "Upgrade-Insecure-Requests": os.getenv('GOOGLE_IMAGE_UPGRADE_INSECURE_REQUESTS'),
                   }

        r = requests.post(self.googlePage, files=self.multipart, allow_redirects=False)
        fetchUrl = r.headers[os.getenv('GOOGLE_IMAGE_HEADER_TO_KEEP')]
        r = requests.get(fetchUrl, headers=headers)

        content = r.content

        soup = BeautifulSoup(content, features=os.getenv('GOOGLE_IMAGE_SOUP_PARSER'))

        key_words = soup.find(os.getenv('GOOGLE_IMAGE_INPUT_ELEMENT'), attrs={os.getenv('GOOGLE_IMAGE_INPUT_TYPE'): os.getenv('GOOGLE_IMAGE_INPUT_VALUE')})
        key_words = key_words.get('value')
        self.key_words = key_words

    def sendKeyWord(self, token):
        headers = {
            'Authorization': 'Bearer ' + token
        }
        data = {'keyWord': self.key_words}

        requests.post(os.getenv('API_SEND_KEY_WORDS_ADDR'), headers=headers, data=data)

    @staticmethod
    def writeInFiles(file_path, data):
        with open(file_path, "a") as f:
            f.write(data + '\n')
