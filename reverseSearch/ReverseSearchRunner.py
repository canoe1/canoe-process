import os

import requests
from PIL import Image

import MainRunner
from reverseSearch.SearchFromImage import SearchFromImage


class ReverseSearchRunner:

    def __init__(self, enable_aliexpress, file_path, token):
        self.token = token
        self.file_path = file_path
        self.downloadImage()

        self.multipart = {'encoded_image': (os.path.dirname(__file__) + "/image.png", open(os.path.dirname(__file__) + "/image.png", 'rb')), 'image_content': ''}

        self.search_from_image = SearchFromImage(self.multipart)
        self.enable_aliexpress = enable_aliexpress

    def run(self):
        self.search_from_image.get_data()
        self.search_from_image.sendKeyWord(self.token)
        MainRunner.MainRunner.run_single(self.token, self.enable_aliexpress, self.search_from_image.key_words, True)

    def downloadImage(self):
        headers = {
            'Authorization' : 'Bearer ' + self.token
        }
        r = requests.get(os.getenv('API_GET_IMAGE_ADDR'), headers=headers)

        f = open(os.path.dirname(__file__) + "/image.png", "wb")
        f.write(r.content)
        f.close()

    def resize(self):
        image = Image.open(os.path.dirname(__file__) + '/image.png')
        (width, height) = (image.width // 6, image.height // 6)
        new_image = image.resize((width, height))
        new_image.save(os.path.dirname(__file__) + '/image.png')