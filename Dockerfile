FROM python:3.7

WORKDIR /workspace/

COPY . /workspace/

RUN apt update
RUN apt install -y chromium-driver

RUN pip3 install -r requirements.txt

CMD [ "python3", "app.py" ]