import os

from selectorlib import Extractor
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from common_tools import ProductScraper
from dto.ProductDto import ProductDto
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


class AliexpressProductScraper:

    def __init__(self, urls):
        self.no_urls = ProductScraper.countLines(self)
        self.data = [0] * self.no_urls
        self.site_name = "aliexpress"
        self.urls = urls

    currentPath = os.path.dirname(__file__)
    e = Extractor.from_yaml_file(currentPath + '/aliexpressProduct.yml')

    def scrape(self, url, counter, q):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')

        driver = webdriver.Chrome(os.getenv('CHROME_DRIVER_PATH'),
                                  chrome_options=chrome_options)
        driver.get(url)

        dataDict = {}

        try:
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, os.getenv('ALIEXPRESS_DIALOG_CLOSE')))).click()
        except:
            pass

        driver.implicitly_wait(1)
        try:
            dataDict['name'] = driver.find_element_by_xpath(os.getenv('ALIEXPRESS_PRODUCT_NAME')).text
        except:
            dataDict['name'] = os.getenv('ALIEXPRESS_NO_PRODUCT_NAME')

        dataDict['price'] = driver.find_element_by_css_selector(
            os.getenv('ALIEXPRESS_PRODUCT_PRICE')).text
        dataDict['short_description'] = None

        dataDict['image'] = driver.find_element_by_css_selector(
            os.getenv('ALIEXPRESS_PRODUCT_IMAGE')).get_attribute(
            os.getenv('ALIEXPRESS_PRODUCT_IMAGE_LINK'))

        try:
            dataDict['rating'] = driver.find_element_by_css_selector(
                os.getenv('ALIEXPRESS_PRODUCT_RATING')).text
        except Exception:
            dataDict['rating'] = None

        dataDict['availability'] = None
        product = self.adapt_extracted_data(dataDict, url)
        del dataDict
        if product:
            q.put({'counter': counter, 'data': product})

    @staticmethod
    def adapt_extracted_data(dataDict, url):
        dataDict['url'] = url
        dataDict['short_description'] = ProductScraper.formatDesc(dataDict['short_description'])

        dataDict['site'] = 'aliexpress'

        if dataDict['name'] is not None and dataDict['price']:
            return ProductDto(dataDict['name'], dataDict['price'], dataDict['short_description'], dataDict['image'],
                              dataDict['availability'], dataDict['rating'], dataDict['url'], dataDict['site'])
        return None
