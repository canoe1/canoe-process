import time

from aliexpress.AliexpressProduct import AliexpressProductScraper
from aliexpress.AliexpressResearch import AliexpressResearchScraper
from common_tools import ProductScraper, ResearchScraper


class AliexpressRunner:

    def __init__(self, product=None):
        self.startTime = time.time()
        self.product = product
        self.aliexpress_researcher_scraper = AliexpressResearchScraper()
        self.aliexpress_product_scraper = None
        self.urls = None
        self.browsed_product = None

    def run(self):
        self.aliexpress_researcher_scraper.get_data(self.product)
        ResearchScraper.formatFile(self.aliexpress_researcher_scraper)

        self.urls = self.aliexpress_researcher_scraper.urls
        self.aliexpress_product_scraper = AliexpressProductScraper(self.urls)
        ProductScraper.searchFromUrls(self.aliexpress_product_scraper)
        if len(self.aliexpress_product_scraper.data) != 0:
            self.browsed_product = ProductScraper.return_cheapest_product(self.aliexpress_product_scraper)
        ProductScraper.createJSON(self.aliexpress_product_scraper)
        ProductScraper.emptyUrlsFile(self.aliexpress_product_scraper)

        print("total time taken: ", str(time.time() - self.startTime) + " aliexpress " + self.product)

    def run_for_favorite(self, urls_list):
        ResearchScraper.formatFile(self.aliexpress_researcher_scraper, urls_list)
        self.urls = urls_list
        self.aliexpress_product_scraper = AliexpressProductScraper(self.urls)

        ProductScraper.searchFromUrls(self.aliexpress_product_scraper)
        ProductScraper.createJSON(self.aliexpress_product_scraper)
        ProductScraper.emptyUrlsFile(self.aliexpress_product_scraper)