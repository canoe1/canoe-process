import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


class AliexpressResearchScraper:

    def __init__(self):
        self.urls = []
        self.aliexpressPage = os.getenv('ALIEXPRESS_SEARCH_ENDPOINT')
        self.product_cookies = None

    currentPath = os.path.dirname(__file__)

    def get_data(self, product):
        requested_page = self.aliexpressPage + product

        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')

        driver = webdriver.Chrome(os.getenv('CHROME_DRIVER_PATH'), chrome_options=chrome_options)
        driver.get(requested_page)

        WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.CSS_SELECTOR, os.getenv('ALIEXPRESS_DIALOG_CLOSE')))).click()
        counter = 0
        items = driver.find_elements_by_css_selector(os.getenv('ALIEXPRESS_PRODUCT_TITLE'))
        for item in items:
            if counter == 3:
                break
            url = item.get_attribute('href')
            self.urls.append(url)
            counter += 1
