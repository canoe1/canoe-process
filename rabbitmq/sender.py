import json

import pika


def send(ip, port, login, pwd, queue, body):
    credentials = pika.PlainCredentials(login, pwd)
    connection = pika.BlockingConnection(pika.ConnectionParameters(ip, port, '/', credentials))
    properties = pika.BasicProperties(content_type='application/json',
                                      content_encoding='UTF-8',
                                      delivery_mode=1)

    channel = connection.channel()

    channel.queue_declare(queue=queue, durable=True)

    channel.basic_publish(exchange='',
                          routing_key=queue,
                          body=json.dumps(body), properties=properties)
    print(" [x] Sent " + json.dumps(body))

    connection.close()
