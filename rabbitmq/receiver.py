#!/usr/bin/env python
import json

from .sender import send
from MainRunner import MainRunner as MainRunner
from dotenv import load_dotenv, find_dotenv

import pika
import os

load_dotenv(find_dotenv())


def receive(ip, port, login, pwd, queue):
    credentials = pika.PlainCredentials(login, pwd)

    connection = pika.BlockingConnection(pika.ConnectionParameters(ip, port, '/', credentials))

    channel = connection.channel()

    channel.queue_declare(queue=queue, durable=True)

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)

        MainRunner.run(MainRunner(), body)
        type, enable_aliExpress, dict = MainRunner.formatAPIInput(body)

        currentPath = os.path.dirname(__file__)
        with open(currentPath + '/../res.json', 'r') as file:
            data = json.loads(file.read())
            data = {
                "token": dict["token"],
                "data": data
            }

            send(os.getenv('AMQP_IP'),
                 os.getenv('AMQP_PORT'),
                 os.getenv('AMQP_LOGIN'),
                 os.getenv('AMQP_PWD'),
                 os.getenv('AMQP_SEND_QUEUE'),
                 data)

        return body

    channel.basic_consume(queue=queue,
                          auto_ack=True,
                          on_message_callback=callback)

    print('[x] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()
