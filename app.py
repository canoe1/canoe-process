import os
import multiprocessing as mp


from dotenv import load_dotenv, find_dotenv
from rabbitmq import receiver

load_dotenv(find_dotenv())
if __name__ == "__main__":
    mp.set_start_method("spawn")
    receiver.receive(os.getenv('AMQP_IP'),
                    os.getenv('AMQP_PORT'),
                    os.getenv('AMQP_LOGIN'),
                    os.getenv('AMQP_PWD'),
                    os.getenv('AMQP_RECEIVE_QUEUE'))



