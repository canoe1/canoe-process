import json
import os
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from dotenv import load_dotenv, find_dotenv

import MainRunner
from dto.FlightDto import FlightDto
from flights.FlightGetData import FlightGetData
from flights.FlightSetData import FlightSetData

load_dotenv(find_dotenv())


def write_result(flights_list):
    current_dir = os.path.dirname(__file__)
    with open(current_dir + '/../res.json', 'w') as res_file:
        json.dump(flights_list, res_file)
        res_file.close()


class FlightRunner:

    def __init__(self, token):
        self.startTime = time.time()
        self.token = token

        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')

        self.driver = webdriver.Chrome(os.getenv('CHROME_DRIVER_PATH'),
                                       chrome_options=chrome_options)

        self.flight_settings = FlightSetData(self.driver)
        self.flight_get_data = FlightGetData(self.driver)

    def run(self, dataDict):
        self.driver.get("https://www.google.com/flights?hl=fr")

        self.flight_settings.set_one_way()

        main_window_handle = self.driver.current_window_handle

        self.flight_settings.set_passengers(int(dataDict['passenger']), main_window_handle)

        self.driver.switch_to.window(main_window_handle)
        self.flight_settings.set_origin_airport(dataDict['origin_airport'])
        self.flight_settings.set_destination_airport(dataDict['destination_airport'])
        self.flight_settings.set_departure_date(dataDict['departure_date'])
        base_url = self.driver.current_url
        self.driver.find_element_by_xpath('(//*[@data-flt-ve="done"])[3]').click()
        WebDriverWait(self.driver, 5).until(lambda driver: driver.current_url != base_url)
        self.base_url = self.driver.current_url
        self.get_row_result(self.token)

        print("total time taken: ", str(time.time() - self.startTime) + " flights ")

    def get_row_result(self, token):
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '(//ol[@class="gws-flights-results__result-list"]/li)[1]')))
        time.sleep(1)
        flights = []
        bool = True
        row_count = 1
        progress = 100 // 3
        while bool:
            if row_count == 4:
                break
            try:
                self.driver.find_element_by_xpath(
                    '(//ol[@class="gws-flights-results__result-list"]/li)[' + str(row_count) + ']')

                logo = self.flight_get_data.get_logo(row_count)
                price = self.flight_get_data.get_price(row_count)
                d_airport = self.flight_get_data.get_d_airport(row_count)
                d_hour = self.flight_get_data.get_d_hour(row_count)
                duration = self.flight_get_data.get_duration(row_count)
                a_airport = self.flight_get_data.get_a_airport(row_count)
                a_hour = self.flight_get_data.get_a_hour(row_count)
                companies = self.flight_get_data.get_companies(row_count)
                stopover = self.flight_get_data.get_stopover(row_count)
                row_url = self.flight_get_data.get_row_url(row_count, self.base_url)

                tmpFlight = FlightDto(logo, price, d_hour, a_hour, duration, d_airport, a_airport, companies,
                                      stopover, row_url).__dict__
                flights.append(tmpFlight)

                row_count += 1
            except:
                bool = False

            MainRunner.update_progress(token, progress)
        write_result(flights)
