import json
import os
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from dotenv import load_dotenv, find_dotenv

import MainRunner


class FlightSetData:

    def __init__(self, driver):
        self.driver = driver

    def set_one_way(self):
        self.driver.find_element_by_xpath('(//dropdown-menu)[1]').click()
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '((//dropdown-menu)[1]//menu-item)[2]'))).click()
        time.sleep(1)

    def set_passengers(self, passenger_number, passenger_window):
        self.driver.find_element_by_xpath('//div[@id="flt-pax-button"]').click()
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//div[@class="gws-flights__modal-dialog"]')))
        self.driver.switch_to.window(passenger_window)
        for i in range(1, passenger_number):
            element = self.driver.find_element_by_xpath('(//div[@class="gws-flights-widgets-numberpicker__flipper-shadow"])[2]')
            self.driver.execute_script("arguments[0].click();", element)
            time.sleep(0.5)
        self.driver.find_element_by_xpath('//div[@class="gws-flights__dialog-button gws-flights__dialog-primary-button"]').click()
        #time.sleep(1)

    def set_origin_airport(self, origin_airport):
        self.driver.find_element_by_xpath('(//*[@data-flt-ve="origin_airport"])[8]').click()
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="sb_ifc50"]/input'))).click()

        origin_airport_elt = self.driver.find_element_by_xpath('//*[@id="sb_ifc50"]/input')
        origin_airport_elt.clear()
        self.driver.implicitly_wait(5)
        origin_airport_elt.send_keys(origin_airport)
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '(//*[@id="sbse0"]//*[@class="fsapp-option-content"])[1]'))).click()

    def set_destination_airport(self, destination_airport):
        self.driver.find_element_by_xpath('(//*[@data-flt-ve="destination_airport"])[8]').click()
        destination_airport_elt = self.driver.find_element_by_xpath('//*[@id="sb_ifc50"]/input')
        destination_airport_elt.send_keys(destination_airport)
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '(//*[@id="sbse0"]//*[@class="fsapp-option-content"])[1]'))).click()

    def set_departure_date(self, departure_date):
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '(//*[@data-flt-ve="departure_date"])[9]'))).click()
        time.sleep(1)
        departure_date_elt = self.driver.find_element_by_xpath('(//date-input)[1]//input')
        departure_date_elt.send_keys(departure_date)
        self.driver.implicitly_wait(5)
        departure_date_elt.send_keys(Keys.ENTER)

    def set_arrival_date(self, arrival_date):
        arrival_date_elt = self.driver.find_element_by_xpath('(//date-input)[2]//input')
        arrival_date_elt.send_keys(arrival_date)
        self.driver.implicitly_wait(5)
        arrival_date_elt.send_keys(Keys.ENTER)