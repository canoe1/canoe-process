class FlightGetData:

    def __init__(self, driver):
        self.driver = driver

    def get_logo(self, row_count):
        logo_url = self.driver.find_element_by_xpath(
            '(//ol[@class="gws-flights-results__result-list"]/li)[' + str(
                row_count) + ']//div[@class="gws-flights-results__itinerary-logo gws-flights__flex-box gws-flights__align-center"]/img').get_attribute(
            'src')
        return logo_url

    def get_price(self, row_count):
        try:
            price = self.driver.find_element_by_xpath(
                '(//ol[@class="gws-flights-results__result-list"]/li)[' + str(
                    row_count) + ']//div[@class="flt-subhead1 gws-flights-results__price gws-flights-results__cheapest-price"]').text
        except:
            price = self.driver.find_element_by_xpath(
                '(//ol[@class="gws-flights-results__result-list"]/li)[' + str(
                    row_count) + ']//div[@class="flt-subhead1 gws-flights-results__price"]').text

        return price

    def get_d_hour(self, row_count):
        d_hour = self.driver.find_element_by_xpath(
            '((//ol[@class="gws-flights-results__result-list"]/li)[' + str(
                row_count) + ']//div[@class="gws-flights-results__times flt-subhead1"]/span)[1]/span/span').text
        return d_hour

    def get_a_hour(self, row_count):
        a_hour = self.driver.find_element_by_xpath(
            '((//ol[@class="gws-flights-results__result-list"]/li)[' + str(
                row_count) + ']//div[@class="gws-flights-results__times flt-subhead1"]/span)[2]/span/span').text
        return a_hour

    def get_duration(self, row_count):
        duration = self.driver.find_element_by_xpath(
            '(//ol[@class="gws-flights-results__result-list"]/li)[' + str(
                row_count) + ']//div[@class ="gws-flights-results__duration flt-subhead1Normal"]').text
        return duration

    def get_d_airport(self, row_count):
        d_airport = self.driver.find_element_by_xpath(
            '((//ol[@class="gws-flights-results__result-list"]/li)[' + str(
                row_count) + ']//div[@class="gws-flights-results__airports flt-caption"]/span)[1]').text
        return d_airport

    def get_a_airport(self, row_count):
        a_airport = self.driver.find_element_by_xpath(
            '((//ol[@class="gws-flights-results__result-list"]/li)[' + str(
                row_count) + ']//div[@class="gws-flights-results__airports flt-caption"]/span)[2]').text
        return a_airport

    def get_companies(self, row_count):
        companies = []
        bool = True
        company_count = 1
        while bool:
            try:
                if company_count == 1:
                    company_elt = self.driver.find_element_by_xpath(
                        '(//ol[@class="gws-flights-results__result-list"]/li)[' + str(
                            row_count) + ']//div[@class="gws-flights-results__carriers gws-flights__ellipsize gws-flights__flex-box gws-flights__align-center flt-caption"]//span/span[' + str(
                            company_count) + ']/span').text
                else:
                    company_elt = self.driver.find_element_by_xpath(
                        '(//ol[@class="gws-flights-results__result-list"]/li)[' + str(
                            row_count) + ']//div[@class="gws-flights-results__carriers gws-flights__ellipsize gws-flights__flex-box gws-flights__align-center flt-caption"]//span/span[' + str(
                            company_count) + ']/span[2]').text
                companies.append(company_elt)
                company_count += 1
            except:
                bool = False
                pass
        return companies

    def get_stopover(self, row_count):

        stopover_airport_list = []

        stopover_amount = self.driver.find_element_by_xpath(
            '((//ol[@class="gws-flights-results__result-list"]/li)[' + str(
                row_count) + ']//div[@class="gws-flights-results__itinerary-stops gws-flights__ellipsize"])//span[1]').text
        stopover_amount = stopover_amount.split(' ')
        if len(stopover_amount[0]) is 1 and stopover_amount[0].isdigit():
            try:
                var = self.driver.find_element_by_xpath(
                    '((//ol[@class="gws-flights-results__result-list"]//li)[' + str(
                        row_count) + ']//div[@class="gws-flights-results__layover-time flt-caption"])//span/span[1]/span').text
                for i in range(int(stopover_amount[0])):
                    stopover_airport_list.append(self.get_airport_stopover(row_count, i + 1))
            except:
                pass

        return stopover_airport_list

    def get_airport_stopover(self, row_count, index):
        if index == 1:
            airport = self.driver.find_element_by_xpath(
                '(//ol[@class="gws-flights-results__result-list"]//li)[' + str(
                    row_count) + ']//div[@class="gws-flights-results__layover-time flt-caption"]//span/span[' + str(
                    index) + ']/span').text
        else:
            airport = self.driver.find_element_by_xpath(
                '(//ol[@class="gws-flights-results__result-list"]//li)[' + str(
                    row_count) + ']//div[@class="gws-flights-results__layover-time flt-caption"]//span/span[' + str(
                    index) + ']/span[2]').text

        return airport

    def get_row_url(self, row_count, url):
        row_url = self.driver.find_element_by_xpath(
            '(//ol[@class="gws-flights-results__result-list"]/li)[' + str(
                row_count) + ']').get_attribute("data-fp")
        return self.build_result_url(row_url, url)

    def build_result_url(self, row_url, url):
        ind = url.index(';')
        return url[:ind] + '.' + row_url + url[ind:]
