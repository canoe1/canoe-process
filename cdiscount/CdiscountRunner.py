import time

from cdiscount.CdiscountProduct import CdiscountProductScraper
from cdiscount.CdiscountResearch import CdiscountResearchScraper
from common_tools import ProductScraper, ResearchScraper


class CdiscountRunner:

    def __init__(self, product=None):
        self.startTime = time.time()
        self.product = product
        self.cdiscount_researcher_scraper = CdiscountResearchScraper()
        self.cdiscount_product_scraper = None
        self.urls = None
        self.browsed_product = None

    def run(self):
        self.cdiscount_researcher_scraper.get_data(self.product)
        ResearchScraper.formatFile(self.cdiscount_researcher_scraper)

        self.urls = self.cdiscount_researcher_scraper.urls
        product_cookies = self.cdiscount_researcher_scraper.product_cookies
        self.cdiscount_product_scraper = CdiscountProductScraper(self.urls, product_cookies)
        ProductScraper.searchFromUrls(self.cdiscount_product_scraper)
        if len(self.cdiscount_product_scraper.data) != 0:
            self.browsed_product = ProductScraper.return_cheapest_product(self.cdiscount_product_scraper)
        ProductScraper.createJSON(self.cdiscount_product_scraper)
        ProductScraper.emptyUrlsFile(self.cdiscount_product_scraper)

        print("total time taken: ", str(time.time() - self.startTime) + " cdiscount " + self.product)

    def run_for_favorite(self, urls_list):
        ResearchScraper.formatFile(self.cdiscount_researcher_scraper, urls_list)
        self.urls = urls_list
        self.cdiscount_product_scraper = CdiscountProductScraper(self.urls)
        ProductScraper.searchFromUrls(self.cdiscount_product_scraper)
        ProductScraper.createJSON(self.cdiscount_product_scraper)
        ProductScraper.emptyUrlsFile(self.cdiscount_product_scraper)
