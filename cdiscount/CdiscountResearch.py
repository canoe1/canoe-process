import os
import requests
from bs4 import BeautifulSoup
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


class CdiscountResearchScraper:

    def __init__(self):
        self.urls = []
        self.cdiscountPage = os.getenv('CDISCOUNT_SEARCH_ENDPOINT')
        self.product_cookies = None

    currentPath = os.path.dirname(__file__)

    def get_data(self, product):
        headers = {'Cache-control': os.getenv('CDISCOUNT_CACHE_CONTROL'),
                   'Content-Type': os.getenv('CDISCOUNT_CONTENT_TYPE'),
                   "User-Agent": os.getenv('USER_AGENT'),
                   'Accept': os.getenv('CDISCOUNT_ACCEPT'),
                   'Accept-language': os.getenv('CDISCOUNT_ACCEPT_LANGUAGE')
                   }

        requested_page = self.cdiscountPage + product + '.html'
        session = requests.Session()

        r = session.get(requested_page, headers=headers)
        self.product_cookies = session.cookies.get_dict()
        content = r.content
        print(r.status_code)
        print(r.content)

        soup = BeautifulSoup(content, features=os.getenv('CDISCOUNT_SOUP_PARSER'))
        counter = 0

        for d in soup.findAll(os.getenv('CDISCOUNT_PRODUCT_CARD_ELEMENT'), attrs={os.getenv('CDISCOUNT_PRODUCT_CARD_SELECTOR_TYPE'): os.getenv('CDISCOUNT_PRODUCT_CARD_SELECTOR_VALUE')}):
            if counter == 3:
                break
            else:
                url = d.find(os.getenv('CDISCOUNT_FIRST_LAYER_SELECTOR_PRODUCT'))
                url = d.find(os.getenv('CDISCOUNT_SECOND_LAYER_SELECTOR_PRODUCT'))
                url = url.get('href')
                counter += 1

                if url is not None:
                    self.urls.append(url)
