import os

from selectorlib import Extractor

from common_tools import ProductScraper
from dto.ProductDto import ProductDto
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


class CdiscountProductScraper:

    def __init__(self, urls, cookies=None):
        self.no_urls = ProductScraper.countLines(self)
        self.data = [0] * self.no_urls
        self.site_name = "cdiscount"
        self.cookies = cookies
        self.urls = urls
        self.headers = {
            'Cache-control': os.getenv('CDISCOUNT_CACHE_CONTROL'),
            'Content-Type': os.getenv('CDISCOUNT_CONTENT_TYPE'),
            "User-Agent": os.getenv('USER_AGENT'),
            'Accept': os.getenv('CDISCOUNT_ACCEPT'),
            'Accept-language': os.getenv('CDISCOUNT_ACCEPT_LANGUAGE')
        }

    currentPath = os.path.dirname(__file__)
    e = Extractor.from_yaml_file(currentPath + '/cdiscountProduct.yml')

    def scrape(self, url, counter, q):

        r = ProductScraper.runRequest(self, url)

        dataDict = CdiscountProductScraper.e.extract(r.text)
        product = self.adapt_extracted_data(dataDict, url)

        if product:
            q.put({'counter': counter, 'data': product})

    @staticmethod
    def adapt_extracted_data(dataDict, url):
        dataDict['url'] = url
        dataDict['short_description'] = ProductScraper.formatDesc(dataDict['short_description'])

        dataDict['site'] = 'cdiscount'

        if dataDict['availability'] is None:
            dataDict['availability'] = "En Stock"

        if dataDict['name'] is not None and dataDict['price']:
            return ProductDto(dataDict['name'], dataDict['price'], dataDict['short_description'], dataDict['image'],
                              dataDict['availability'], dataDict['rating'], dataDict['url'], dataDict['site'])
        return None
