import os

from selectorlib import Extractor

from common_tools import ProductScraper
from dto.ProductDto import ProductDto
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


class BoulangerProductScraper:

    def __init__(self, urls, cookies=None):
        self.no_urls = ProductScraper.countLines(self)
        self.data = [0] * self.no_urls
        self.site_name = "boulanger"
        self.cookies = cookies
        self.urls = urls
        self.headers = {
            'Authority': os.getenv('BOULANGER_AUTHORITY'),
            'Pragma': os.getenv('BOULANGER_PRAGMA'),
            'Cache-control': os.getenv('BOULANGER_CACHE_CONTROL'),
            'dDnt': os.getenv('BOULANGER_DNT'),
            'Upgrade-insecure-requests': os.getenv('BOULANGER_UPGRADE_INSECURE_REQUEST'),
            "User-Agent": os.getenv('USER_AGENT'),
            'Accept': os.getenv('BOULANGER_ACCEPT'),
            'Accept-language': os.getenv('BOULANGER_ACCEPT_LANGUAGE')
        }

    currentPath = os.path.dirname(__file__)
    e = Extractor.from_yaml_file(currentPath + '/boulangerProduct.yml')

    def scrape(self, url, counter, q):

        r = ProductScraper.runRequest(self, url)
        dataDict = BoulangerProductScraper.e.extract(r.text)

        product = self.adapt_extracted_data(dataDict, url)
        if product:
            q.put({'counter': counter, 'data': product})

    @staticmethod
    def adapt_extracted_data(dataDict, url):
        dataDict['url'] = url

        dataDict['site'] = 'boulanger'

        if 'Livraison' in dataDict['availability']:
            dataDict['availability'] = 'En Stock'

        dataDict['short_description'] = ProductScraper.formatDesc(dataDict['short_description'])



        if dataDict['name'] is not None and dataDict['price']:
            return ProductDto(dataDict['name'], dataDict['price'], dataDict['short_description'], dataDict['image'],
                              dataDict['availability'], dataDict['rating'], dataDict['url'], dataDict['site'])
        return None
