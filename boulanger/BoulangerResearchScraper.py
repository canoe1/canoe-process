import os
import requests
from bs4 import BeautifulSoup
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())



class BoulangerResearchScraper:

    def __init__(self):
        self.urls = []
        self.boulangerPage = os.getenv('BOULANGER_SEARCH_ENDPOINT')
        self.product_cookies = None

    currentPath = os.path.dirname(__file__)

    def get_data(self, product):
        headers = {'Authority': os.getenv('BOULANGER_AUTHORITY'),
                   'Pragma': os.getenv('BOULANGER_PRAGMA'),
                   'Cache-control': os.getenv('BOULANGER_CACHE_CONTROL'),
                   'dDnt': os.getenv('BOULANGER_DNT'),
                   'Upgrade-insecure-requests': os.getenv('BOULANGER_UPGRADE_INSECURE_REQUEST'),
                   "User-Agent": os.getenv('USER_AGENT'),
                   'Accept': os.getenv('BOULANGER_ACCEPT'),
                   'Accept-language': os.getenv('BOULANGER_ACCEPT_LANGUAGE')
                   }

        requested_page = self.boulangerPage + product

        session = requests.Session()

        r = session.get(requested_page, headers=headers)
        self.product_cookies = session.cookies.get_dict()

        content = r.content
        soup = BeautifulSoup(content, features=os.getenv('BOULANGER_SOUP_PARSER'))
        counter = 0

        for d in soup.findAll(os.getenv('BOULANGER_PRODUCT_CARD_ELEMENT'), attrs={os.getenv('BOULANGER_PRODUCT_CARD_SELECTOR_TYPE'): os.getenv('BOULANGER_PRODUCT_CARD_SELECTOR_VALUE')}):
            if counter == 3:
                break
            else:
                url = d.find(os.getenv('BOULANGER_FIRST_LAYER_SELECTOR_PRODUCT'))
                url = d.find(os.getenv('BOULANGER_SECOND_LAYER_SELECTOR_PRODUCT'))
                url = 'https://www.boulanger.com/' + url.get('href')
                counter += 1

                if url is not None:
                    self.urls.append(url)
