import time

from boulanger.BoulangerProductScraper import BoulangerProductScraper
from boulanger.BoulangerResearchScraper import BoulangerResearchScraper
from common_tools import ProductScraper, ResearchScraper


class BoulangerRunner:

    def __init__(self, product=None):
        self.startTime = time.time()
        self.product = product
        self.boulanger_researcher_scraper = BoulangerResearchScraper()
        self.boulanger_product_scraper = None
        self.urls = None
        self.browsed_product = None

    def run(self):
        self.boulanger_researcher_scraper.get_data(self.product)
        ResearchScraper.formatFile(self.boulanger_researcher_scraper)

        self.urls = self.boulanger_researcher_scraper.urls
        product_cookies = self.boulanger_researcher_scraper.product_cookies
        self.boulanger_product_scraper = BoulangerProductScraper(self.urls, product_cookies)
        ProductScraper.searchFromUrls(self.boulanger_product_scraper)
        if len(self.boulanger_product_scraper.data) != 0:
            self.browsed_product = ProductScraper.return_cheapest_product(self.boulanger_product_scraper)
        ProductScraper.createJSON(self.boulanger_product_scraper)
        ProductScraper.emptyUrlsFile(self.boulanger_product_scraper)


        print("total time taken: ", str(time.time() - self.startTime) + " boulanger " + self.product)

    def run_for_favorite(self, urls_list):
        ResearchScraper.formatFile(self.boulanger_researcher_scraper, urls_list)
        self.urls = urls_list
        self.boulanger_product_scraper = BoulangerProductScraper(self.urls)
        ProductScraper.searchFromUrls(self.boulanger_product_scraper)
        ProductScraper.createJSON(self.boulanger_product_scraper)
        ProductScraper.emptyUrlsFile(self.boulanger_product_scraper)