import pandas as pd
import numpy as np


def formatFile(research_name, url_list=None):
    if url_list:
        df = pd.DataFrame({'URLS': url_list})
    else:
        df = pd.DataFrame({'URLS': research_name.urls})
    np.savetxt(research_name.currentPath + '/urls.txt', df.values, fmt='%s')


class ResearchScraper:
    pass
