import json
import os
import threading
from multiprocessing import Manager

import requests

from dto.ProductDto import ProductDto
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


def runRequest(product_object, url):
    if product_object.cookies is None:
        r = requests.get(url, headers=product_object.headers)
    else:
        r = requests.get(url, headers=product_object.headers, cookies=product_object.cookies)
    return r


def searchFromUrls(product_object):
    m = Manager()
    q = m.Queue()
    p = {}

    counter = 0
    for url in product_object.urls:
        p[counter] = threading.Thread(target=product_object.scrape, args=(url, counter, q))
        p[counter].start()
        counter += 1
    for i in range(0, counter):
        p[i].join()

    while q.empty() is not True:
        queue_top = q.get()
        if queue_top.get('counter') < len(product_object.data):
            product_object.data[queue_top.get('counter')] = queue_top.get('data').__dict__


def createJSON(product_object):
    with open(product_object.currentPath + '/output.json', 'w') as outfile:
        json.dump(product_object.data, outfile)


def return_cheapest_product(product_object):
    while 0 in product_object.data:
        product_object.data.remove(0)
    product_object.data.sort(key=ProductDto.get_price)
    if len(product_object.data) > 0 and product_object.data[0] is not None:
        return product_object.data[0]
    return None

def emptyUrlsFile(product_object):
    file = open(product_object.currentPath + '/urls.txt', "w")
    file.write("")
    file.close()


def countLines(product_object):
    return len(open(product_object.currentPath + '/urls.txt').readlines())


def formatDesc(description):
    if description is None or description is '':
        return os.getenv('COMMON_DESC_NOT_FOUND')
    elif description and len(description) >= 80:
        return description[0:80] + os.getenv('COMMON_DESC_TOO_LONG')
    else:
        return description


class ProductScraper:
    pass
